<!DOCTYPE html>

<html >
<head>
  <meta charset="utf-8" />
  <title>DP3</title>
  <link href="estilo.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>DATOS PERSONALES 3 (RESULTADO)</h1>
<?php
    function muestraNomApe($nombre, $apellidos) {
	if (isset($_REQUEST[$nombre])){ 
	    $nomTxt =strip_tags(trim($_REQUEST[$nombre]));
        } else {
            $nomTxt = "";
        }
        
        if(isset($_REQUEST[$apellidos])) {
            $apeTxt =strip_tags(trim($_REQUEST[$apellidos]));
        } else {
            $apeTxt = "";
        }
	
	if ($nomTxt=="" || $apeTxt ==""){
	   echo ("<p class=\"aviso\">Debe escribir un nombre y un apellido al menos en la caja de texto</p>\n");
	} else {
	    echo("<p>Tu nombre es <strong>$nomTxt</strong>.</p>");
	    echo("<p>Tus apellidos son <strong>$apeTxt</strong>.</p>");
	}
	
        echo("<p><a href=\"dp3.html\">Volver a la página anterior</a></p>");
    }
    
    muestraNomApe("nombre","apellidos");
?>

</body>
</html>

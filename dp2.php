<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
      <title>DP2</title>
      <link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
    <h1>DATOS PERSONALES 2 (RESULTADO)</h1>
    <?php
    function muestraNombre($nombre) {
	if (isset($_REQUEST[$nombre])){ 
	    $texto =strip_tags(trim($_REQUEST[$nombre]));
	} 
	
	if ($texto==""){
	   echo ("<p class=\"aviso\">Debe escribir un nombre en la caja de texto</p>\n");
	} else {
	    echo("<p>Su nombre es <strong>$texto</strong>.</p>");
	}
        
	echo("<p><a href=\"dp2.html\">Volver a la página anterior</a></p>");
    }

    muestraNombre("nombre");
    ?>

    </body>
</html>

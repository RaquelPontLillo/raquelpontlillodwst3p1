<!DOCTYPE html>

<html >
    <head>
        <meta charset="utf-8" />
        <title>DP7</title>
        <link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <h1>DATOS PERSONALES 7 (RESULTADO)</h1>
        <?php

        function comprobarFormulario($datos) {
            if (isset($_REQUEST[$datos])) {
                $datosTxt = strip_tags(trim($_REQUEST[$datos]));
            } else {
                $datosTxt = "";
            }
            return $datosTxt;
        }

        $nombre = comprobarFormulario("nombre");
        $apellidos = comprobarFormulario("apellidos");
        $edad = comprobarFormulario("edad");
        $peso = comprobarFormulario("peso");
        $sexo = comprobarFormulario("genero");
        $estadoCivil = comprobarFormulario("estadoCivil");
        $cine = comprobarFormulario("cine");
        $deporte = comprobarFormulario("deporte");
        $literatura = comprobarFormulario("literatura");
        $musica = comprobarFormulario("musica");
        $tebeos = comprobarFormulario("tebeos");
        $television = comprobarFormulario("television");

        
        /*
         * Comprobación de que el nombre está introducido
         */
        if ($nombre == "") {
            echo ("<p class=\"aviso\">Debe escribir un nombre en la caja de texto</p>\n");
        } else {
            echo("<p>Su nombre es <strong>$nombre</strong>.</p>");
        }

        /*
         * Comprobación de que los apellidos están introducidos
         */
        if ($apellidos == "") {
            echo ("<p class=\"aviso\">Debe escribir un apellido al menos en la caja de texto</p>\n");
        } else {
            echo("<p>Tus apellidos son <strong>$apellidos</strong>.</p>");
        }

        /*
         * Comprobación de la edad
         */
        switch ($edad) {
            case 1:
                echo ("<p>Tienes <strong>menos de 20</strong> años.</p>\n");
                break;
            case 2:
                echo ("<p>Tienes <strong>entre 20 y 39</strong> años.</p>\n");
                break;
            case 3:
                echo ("<p>Tienes <strong>entre 40 y 60</strong> años.</p>\n");
                break;
            case 4:
                echo ("<p>Tienes <strong>más de 60</strong> años.</p>\n");
                break;
            default:
                echo ("<p class=\"aviso\">Debes seleccionar tu edad.</p>\n");
                break;
        }

        /*
         * Comprobación del peso
         */
        if ($peso == "") {
            print "<p class=\"aviso\">Debes escribir tu peso.</p>";
        } elseif (!is_numeric($peso)) {
            print "<p class=\"aviso\">El peso debe ser numérico.</p>";
        } elseif ($peso < 0) {
            print "<p class=\"aviso\">El peso debe ser un número positivo.</p>";
        } else {
            print "<p>Tu peso es de <strong>$peso</strong> Kg.</p>";
        }

        /*
         * Comprobación del sexo
         */
        switch ($sexo) {
            case "hombre":
                echo ("<p>Eres un <strong>hombre ♂</strong>.</p>\n");
                break;
            case "mujer":
                echo ("<p>Eres una <strong>mujer ♀</strong>.</p>\n");
                break;
            default:
                echo ("<p class=\"aviso\">Debes seleccionar tu sexo.</p>\n");
                break;
        }

        /*
         * Comprobación del estado civil
         */
        switch ($estadoCivil) {
            case "soltero":
                echo ("<p>Tu estado civil es <strong>soltero</strong>.</p>\n");
                break;
            case "casado":
                echo ("<p>Tu estado civil es <strong>casado</strong>.</p>\n");
                break;
            case "otro":
                echo ("<p>Tu estado civil no es <strong>ni soltero ni casado</strong>.</p>\n");
                break;
            default:
                echo ("<p class=\"aviso\">Debes seleccionar tu estado civil.</p>\n");
                break;
        }

        /*
         * Comprobación de aficiones
         */
        if ($cine != "on" && $deporte != "on" && $literatura != "on" &&
                $musica != "on" && $tebeos != "on" && $television != "on") {
            print "<p class=\"aviso\">Debes seleccionar alguna afición.</p>\n";
        } else {
            print "<p>Le gusta: \n";
            if ($cine == "on") {
                print "  <strong>el cine</strong>, \n";
            }
            if ($deporte == "on") {
                print "  <strong>el deporte</strong>, \n";
            }
            if ($literatura == "on") {
                print "  <strong>la literatura</strong>, \n";
            }
            if ($musica == "on") {
                print "  <strong>la música</strong>, \n";
            }
            if ($tebeos == "on") {
                print "  <strong>los tebeos</strong>, \n";
            }
            if ($television == "on") {
                print "  <strong>la televisión</strong> \n";
            }
            print "</p>\n";
        }
        echo ("<p><a href=\"dp7.html\">Volver a la página anterior</a></p>\n");
        ?>

    </body>
</html>

<!DOCTYPE html>

<html >
    <head>
        <meta charset="utf-8" />
        <title>DP6</title>
        <link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <h1>DATOS PERSONALES 6 (RESULTADO)</h1>
        <?php

        function muestraEdad($edad) {
            if (isset($_REQUEST[$edad])) {
                $edaTxt = strip_tags(trim($_REQUEST[$edad]));
            } else {
                $edaTxt = "";
            }

            switch ($edaTxt) {
                case 1:
                    echo ("<p>Tienes <strong>menos de 20</strong> años.</p>\n");
                    break;
                case 2:
                    echo ("<p>Tienes <strong>entre 20 y 39</strong> años.</p>\n");
                    break;
                case 3:
                    echo ("<p>Tienes <strong>entre 40 y 60</strong> años.</p>\n");
                    break;
                case 4:
                    echo ("<p>Tienes <strong>más de 60</strong> años.</p>\n");
                    break;
                default:
                    echo ("<p class=\"aviso\">Debes seleccionar una de las opciones.</p>\n");
                    break;
            }
            echo ("<p><a href=\"dp6.html\">Volver a la página anterior</a></p>\n");
        }

        muestraEdad("edad");
        ?>

    </body>
</html>

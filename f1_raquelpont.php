<!DOCTYPE html>

<html >
    <head>
        <meta charset="utf-8" />
        <title>DP7</title>
        <link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <h1>FORMULARIO AMPLIADO 1 (RESULTADO)</h1>
        <?php
        
        function control($datos) {
            if (isset($_REQUEST[$datos])) {
                $datosTxt = strip_tags(trim($_REQUEST[$datos]));
            } else {
                $datosTxt = "";
            }
            return $datosTxt;
        }
        
        function validar($entrada, $botones) {
            $entrada = control($entrada);
            $botones = control($botones);
            switch ($botones) {
                case "email":
                    $validacion = filter_var($entrada, FILTER_VALIDATE_EMAIL);
                    break;
                case "dni":
                    $letra = substr($entrada, -1);
                    $numeros = substr($entrada, 0, -1);
                    if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra 
                            && strlen($letra) == 1 && strlen ($numeros) == 8 ){
                        $validacion = true;
                    }else {
                        $validacion = false;
                    }
                    break;
                case "numeros":
                    $validacion = is_numeric($entrada);
                    break;
                
            }
            return $validacion;
        }

        if (control("validacion")) {
            $comprobar = validar("texto","validacion");
            if ($comprobar) {
                echo ("Dato correcto.");
            } else {
                echo ("Dato incorrecto.");
            }
        } else {
            echo ("<p class=\"aviso\">Debes seleccionar un filtro.</p>\n");
        }
 
        echo ("<p><a href=\"f1_raquelpont.html\">Volver a la página anterior</a></p>\n");
        ?>

    </body>
</html>

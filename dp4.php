<!DOCTYPE html>

<html >
    <head>
        <meta charset="utf-8" />
        <title>DP4</title>
        <link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <h1>DATOS PERSONALES 4 (RESULTADO)</h1>
        <?php

        function muestraSexo($sexo) {
            if (isset($_REQUEST[$sexo])) {
                $sexTxt = strip_tags(trim($_REQUEST[$sexo]));
            } else {
                $sexTxt = "";
            }

            switch ($sexTxt) {
                case "hombre":
                    echo ("<p>Eres un <strong>hombre ♂</strong>.</p>\n");
                    break;
                case "mujer":
                    echo ("<p>Eres una <strong>mujer ♀</strong>.</p>\n");
                    break;
                default:
                    echo ("<p class=\"aviso\">Debes seleccionar una de las opciones.</p>\n");
                    break;
            }

            echo ("<p><a href=\"dp4.html\">Volver a la página anterior</a></p>\n");
        }

        muestraSexo("genero");
        ?>

    </body>
</html>
